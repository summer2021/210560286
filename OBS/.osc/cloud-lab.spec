#
# spec file for package Cloud-Lab
#
# -- Copyright omitted --

Name:           cloud-lab
Version:        0.5 
Release:        0 
License:        GPL-3.0 
Group:          Education 
Summary:        Docker based Cloud Lab Center 
Url:            http://tinylab.org/cloud-lab/ 
Source0:	cloud-lab.tar.gz
Source1:	cloudlab-choose.desktop
Source2:	cloudlab-run.desktop
Source3:	cloudlab.png
Source4:	cloudlab.svg
Source5:	cloudlab.xml
Source6:	cloud-lab.sh
BuildRequires:  git
Requires:  	docker
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description 
Docker based Cloud Lab Center, with noVNC and Gateone attachable LXDE Desktop and SSH Terminal.

%prep 
%setup -n %{name}-master
# %setup -n %{name}

%build 

%install
%define _cloudlabroot "cloud-lab-master"
install -dm777 "%{buildroot}/opt/%{name}/"
cp -rp "%{_builddir}/%{_cloudlabroot}/." "%{buildroot}/opt/%{name}/"

install -Dm755 "%{SOURCE1}" "%{buildroot}/usr/share/applications/cloud-lab-choose.desktop"
install -Dm755 "%{SOURCE2}" "%{buildroot}/usr/share/applications/cloud-lab.desktop"
install -Dm744 "%{SOURCE3}" "%{buildroot}/usr/share/icons/cloudlab.png"
install -Dm744 "%{SOURCE4}" "%{buildroot}/usr/share/icons/hicolor/scalable/mimetypes/cloudlab.svg"
install -Dm755 "%{SOURCE5}" "%{buildroot}/usr/share/metainfo/org.tinylab.cloud_lab.metainfo.xml"
install -Dm755 "%{SOURCE6}" "%{buildroot}/usr/bin/cloud-lab"
install -Dm644 "%{_builddir}/%{_cloudlabroot}/COPYING" "%{buildroot}/usr/share/licenses/%{name}/LICENSE"


%files 
%{_bindir}/*
/opt/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/icons/cloudlab.png
%{_datadir}/icons/hicolor
%{_datadir}/icons/hicolor/scalable/mimetypes/cloudlab.svg
%{_datadir}/metainfo/org.tinylab.cloud_lab.metainfo.xml
%{_datadir}/licenses/%{name}

# %doc README LICENSE

%changelog
