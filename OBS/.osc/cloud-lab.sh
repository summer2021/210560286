#!/bin/sh

show_help() {
	echo "Usage: $0 action args"
	echo "Example: $0 choose linux-lab"
	exit -1
}

if [ -z ${cloud_lab_root}]; then
	cloud_lab_root="/opt/cloud-lab"
fi

if [ ! -d ${cloud_lab_root} ]; then
	echo "Cannot find Cloud-Lab root directory."
	exit -1
fi

cd ${cloud_lab_root}
i=0
args=""
for arg in $@; do
	i=$i+1
	if [[ $i -le 1 ]]; then
		continue
	fi
	args="$args $arg"
done

script="${cloud_lab_root}/tools/docker/$1"
if [ ! -f $script ]; then
	show_help
	exit -1
fi
echo "==> $script $args"
sh $script $args

