# Cloud-Lab Setup 安装脚本

## Linux 平台

### 准备

+ sudo
+ curl / wget

### 安装

```sh
curl -sSLO https://gitee.com/nebell/shell-script-test/raw/master/cloud-lab-setup.sh
chmod +x cloud-lab-setup.sh
sh cloud-lab-setup.sh
```

安装脚本会默认安装一个便于使用 cloud-lab docker tools 的脚本工具在 `/usr/bin/cloud-lab`；在 `cloud-lab-setup.sh` 脚本中有其默认的获取地址`_launcher_url="https://gitee.com/nebell/shell-script-test/raw/master/cloud-lab.sh"`。

如果要将本仓库进行移动，需要更改`cloud-lab-setup.sh` 脚本中有其默认的获取地址`_launcher_url="https://new_url_to/cloud-lab.sh"`。

### 脚本说明

+ `_install_deps_of_linux`函数会根据 Linux 发行版本不同安装对应的依赖

+ `check_lack`进行依赖的检查（通过检查命令检查），并返回缺少的依赖
+ `_config_docker_apt` 配置 Apt 包管理的 Docker 安装源
+ `install_cloudlab` 安装 Cloud-Lab 以及便于使用 Cloud-Lab 的脚本

## Windows 平台

### 准备

+ Powershell

### 安装

可能需要先允许 Powershell 执行脚本

```powershell
Set-ExecutionPolicy Unrestricted
```

然后根据[WSL官方文档](https://docs.microsoft.com/zh-cn/windows/wsl/about)开启WSL2相关内容。

获取[cloud-lab-setup.ps1](https://gitee.com/nebell/shell-script-test/blob/master/cloud-lab-setup.ps1)脚本，进行右键运行。

脚本运行结束后，请**[重启]**系统，确保后续安装的执行。

### 使用

默认安装在 `C:\Program Files\cloud-lab`，需要在 git bash 下进行使用。

## Mac OS 平台

### 准备

+ brew
+ sudo

### 安装

```sh
curl -sSLO https://gitee.com/nebell/shell-script-test/raw/master/cloud-lab-setup.sh
chmod +x cloud-lab-setup.sh
sh cloud-lab-setup.sh
```

需要注意，安装脚本默认使用 brew 安装。在使用 brew 安装 Docker 时，会出现 `$PATH` 中找不到`docker`命令。

则需要用户自己将其加入到`$PATH`中，以下是一个解决方案，仅供参考：

```bash
sudo vim /etc/profile
```

然后，将 Docker 默认的 Path 添加进文件。

```sh
export DOCKER_PATH=/Applications/Docker.app/Contents/Resources/bin/docker
export PATH=".\$PATH:$DOCKER_PATH"
```

按下`ESC`，输入`:wq!`保存退出。

更新 `$PATH` 变量：

```bash
source /etc/profile
```

然后输入 `docker run hello-world` 进行测试。

### 使用

Cloud-Lab 默认安装在 `/Users/用户名/Documents/tinylab/`目录下，该目录会开启大小写敏感。

使用前，确保 Docker 服务已经启动，并且将生成 cloud-lab 的所在的 dmg 进行挂载。

```bash
# 挂载
sudo hdiutil attach -mountpoint "/Users/用户名/Documents/tinylab/cloud-lab" -nobrowse "/Users/用户名/Documents/tinylab/labspace.dmg.sparseimage"
# 开启 Docker 服务
open /Applications/Docker.app
# 进入 Cloud-Lab 文件夹
cd "/Users/用户名/Documents/tinylab/cloud-lab"
```