$CLOUD_LAB_ROOT_DIR = [Environment]::GetEnvironmentVariable("ProgramFiles") + "\cloud-lab"
$LAUNCHER_URL = "https://gitee.com/nebell/shell-script-test/raw/master/cloud-lab.sh"
Function CheckSoftwareInstalled($SoftwareName)
{
    $32BitPrograms = Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*
    $64BitPrograms = Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*
    $programsWithGitInName = ($32BitPrograms + $64BitPrograms) | Where-Object { $null -ne $_.DisplayName -and $_.Displayname.Contains($SoftwareName) }
    $isInstalled = $null -ne $programsWithGitInName
    return $isInstalled
}

Function CheckIfCommandExists ($command) {
    return Get-Command $command -errorAction SilentlyContinue
}

# Check if winget exists and install via winget
Function CheckAndInstallViaWinget($pkgname) {
    if (CheckIfCommandExists "winget")
    {
        $confirmation = Read-Host "Install " + $pkgname + " via winget? [Y/n]"
        if ($confirmation -eq "" -or $confirmation -eq 'Y' -or $confirmation -eq 'y')
        {
            winget install $pkgname
            return $?
        }
    }
    return $false
}

Function RunCommandAsAdmin($command)
{
    Start-Process powershell.exe -Verb Runas -ArgumentList "-Command &; {$command}"
}

Function EnableWSL2()
{
    # Enable wsl and update to wsl2
    Enable-WindowsOptionalFeature -Online -NoRestart -FeatureName Microsoft-Windows-Subsystem-Linux
    $WSL2_UPDATER_URL = "https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi"
    $wslupdater = "$env:temp\wsl-updater.msi"
    Invoke-WebRequest -Uri $WSL2_UPDATER_URL -OutFile $wslupdater
    # Start-Process $wslupdater -Wait -Verb runas
    $InstallAction = New-ScheduledTaskAction -Execute "$wslupdater"
    $StartupTrigger = New-JobTrigger -AtLogOn
    Register-ScheduledTask -Action $InstallAction -Trigger $StartupTrigger -TaskName "Update to WSL2"
    # Register-ScheduledJob -Trigger $StartupTrigger -FilePath $wslupdater -Name "Update to WSL2"
    Write-Host "Please reboot after script finished to update to wsl2."
}

Function Install-Docker
{
    $confirmation = Read-Host "Install docker quietly? [Y/n]"
    if ($confirmation -eq 'n' -or $confirmation -eq 'N')
    {
        Write-Host "Cancel to install docker desktop."
        exit
    }

    # Check and install via winget if winget exists
    if(CheckAndInstallViaWinget "Docker")
    {
        return
    }

    $DOCKER_DESKTOP_INSTALLER_URL = "https://desktop.docker.com/win/stable/amd64/Docker%20Desktop%20Installer.exe"
    $installer = "$env:temp\docker-installer.exe"

    Invoke-WebRequest -Uri $DOCKER_DESKTOP_INSTALLER_URL -OutFile $installer
    Start-Process $installer "install --quiet" -Wait -Verb runas
    Write-Host "Please install wsl2 for docker."
    Write-Host "Please checkout https:/aka.ms/wsl2 or install wsl from command line!!!"
}

Function Install-Git
{
    $confirmation = Read-Host "Install Git? [Y/n]"
    if ($confirmation -eq 'n' -or $confirmation -eq 'N')
    {
        Write-Host "Cancel to install docker desktop."
        exit
    }

    # Get latest download url for git-for-windows 64-bit exe
    $git_url = "https://api.github.com/repos/git-for-windows/git/releases/latest"
    $asset = Invoke-RestMethod -Method Get -Uri $git_url | ForEach-Object assets | Where-Object name -like "*64-bit.exe"
    $installerUrl = $asset.browser_download_url

    # Download installer
    $installer = "$env:temp\$($asset.name)"
    $checkIP = Invoke-WebRequest -Uri "https://pv.sohu.com/cityjson"
    if($checkIP -match "CHINA")
    {
        $installerUrl = "https://mirrors.tuna.tsinghua.edu.cn/github-release/git-for-windows/git/LatestRelease/$($asset.name)"
    }

    Write-Host "Downloading Git from $installerUrl"
    Invoke-WebRequest -Uri $installerUrl -OutFile $installer
    if (!(Test-Path $installer))
    {
        Pause "Failed to download installer of git."
        exit 255
    }

    # Check and install via winget if winget exists
    if(CheckAndInstallViaWinget "Git")
    {
        return
    }

    # Run installer
    $confirmation = Read-Host "Install Git with default configuration quietly? [Y/n]"
    if($confirmation -eq "" -or $confirmation -eq 'Y' -or $confirmation -eq 'y')
    {
        $install_args = "/SP- /VERYSILENT /SUPPRESSMSGBOXES /NOCANCEL /NORESTART /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS"
    } else {
        $install_args = "/NOCANCEL /NORESTART"
    }

    Start-Process -FilePath $installer -ArgumentList $install_args -Wait
}

Function Install-Cloud-Lab
{
    # Create Cloud-Lab dir if not exists
    if (!(Test-Path $CLOUD_LAB_ROOT_DIR))
    {
        New-Item -itemType Directory -Path $CLOUD_LAB_ROOT_DIR
        $Acl = Get-ACL $CLOUD_LAB_ROOT_DIR
        $cloud_lab_user = "users"
        $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($cloud_lab_user,"FullControl","ContainerInherit,Objectinherit","none","Allow")
        $Acl.AddAccessRule($AccessRule)
        Set-Acl $CLOUD_LAB_ROOT_DIR $Acl

    } else {
        Write-Host "$CLOUD_LAB_ROOT_DIR already exists."
        pause "Please uninstall cloud-lab and try again."
        exit 255
    }

    EnableWSL2
    fsutil file SetCaseSensitiveInfo $CLOUD_LAB_ROOT_DIR enable
    if(!$?)
    {
        Write-Host "Failed to set case sensitive info."
        Write-Host "Please reboot and run 'fsutil file SetCaseSensitiveInfo $CLOUD_LAB_ROOT_DIR enable' again."
        $InstallAction = New-ScheduledTaskAction -Execute "fsutil.exe" -Argument "file SetCaseSensitiveInfo $CLOUD_LAB_ROOT_DIR enable"
        $StartupTrigger = New-JobTrigger -AtLogOn
        Register-ScheduledTask -Action $InstallAction -Trigger $StartupTrigger -TaskName "Set CaseSensitive to $CLOUD_LAB_ROOT_DIR"
        Pause
    }

    # clone repo
    $CLOUD_LAB_GIT_REPO = "https://gitee.com/tinylab/cloud-lab/"
    git clone $CLOUD_LAB_GIT_REPO $CLOUD_LAB_ROOT_DIR --config core.autocrlf=true

    # Install Launcher
    $launcher_path = $CLOUD_LAB_ROOT_DIR + "/cloud-lab.sh"
    Invoke-WebRequest -Uri $LAUNCHER_URL -OutFile $launcher_path
}

Function main
{
    if(![System.Environment]::Is64BitProcess)
    {
        Write-Host "Please run on x64 platform."
        return
    }

    if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) 
    { 
        $proc = Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Wait -Verb RunAs 2>&1 > "$env:temp\cloud-lab-setup.log"
        if($proc -ne 0)
        {
            Pause
            Exit
        }

        # add current user to the group docker-users
        RunCommandAsAdmin "Net localgroup docker-users '$env:userdomain/$env:username' /add"
        Pause
        Exit
    }
    
    if(!(CheckSoftwareInstalled 'Git' -or CheckIfCommandExists "git"))
    {
        Write-Host "Git is not installed."
        Install-Git
    }

    # Install Docker for common users
    if (!(CheckSoftwareInstalled 'Docker' -or CheckIfCommandExists "docker"))
    {
        Write-Host "Docker Desktop is not installed."
        Install-Docker
    }

    # Reload the path
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
    Install-Cloud-Lab
}

main
pause