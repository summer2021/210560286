#!/bin/bash

show_help() {
	echo "Usage: $0 action args"
	echo "Example: $0 choose linux-lab"
	exit -1
}

# Check cloud-lab root
if [ -z ${cloud_lab_root}]; then
	cloud_lab_root="/opt/cloud-lab"
fi

if [ ! -d ${cloud_lab_root} ]; then
	echo "Cannot find Cloud-Lab root directory."
	exit -1
fi

# Check docker service
systemctl status docker &> /dev/null
if [ ! $? -eq 0 ]; then
	echo "Docker service is inactive."

	read -p "Start docker service [Y/N] " choice
	if [[ $choice != "Y"  &&  $choice != "y" ]]; then
		echo "Please start the docker service and try again."
		exit -1
	fi

	echo "Starting docker service..."
	systemctl start docker
	if [ ! $? -eq 0 ]; then
		echo "Failed."
		echo "Please start docker service and try again."
		exit -1
	fi
fi

# User is in the group docker or not
if [[ ! $(groups $USER) == *"docker"* ]] ; then
	echo "Current user is not in the group docker."

	read -p "Add current user to the group docker [Y/N] " choice
	if [[ $choice != "Y"  &&  $choice != "y" ]]; then
		echo "Please add the current user to the group docker and try again."
		echo "For example: sudo usermod -aG docker $USER"
		exit -1
	fi

	grep -q docker /etc/group &> /dev/null
	if [[ $? != 0 ]]; then
		echo "Group docker is not existed."
		echo "Adding group docker..."
		sudo groupadd docker
		if [[ $? != 0 ]]; then
			echo "Failed."
			echo "Please add the current user to the group docker and try again."
			echo "For example: sudo usermod -aG docker $USER"
			exit -1
		fi
	fi

	sudo usermod -aG docker $USER
	if [ ! $? -eq 0 ]; then  
		echo "Failed."
		echo "Please add the current user to the group docker and try again."
		echo "For example: sudo usermod -aG docker $USER"
		exit -1
	fi

	# effects immediately
	# newgrp docker &> /dev/null
	printf "Please run `%s` and try again.\n" "newgrp docker"
	exit -1
fi

cd ${cloud_lab_root}
i=0
args=""
for arg in $@; do
	i=$i+1
	if [[ $i -le 1 ]]; then
		continue
	fi
	args="$args $arg"
done

script="${cloud_lab_root}/tools/docker/$1"
if [ ! -f $script ]; then
	show_help
	exit -1
fi
echo "==> $script$args"
sh $script $args