#!/bin/sh
# Inpired by rustup.sh

_name="cloud-lab"
_rootdir="/opt/$_name"
_builddir="/tmp/$_name-build"
_basic_deps="git"
_need_cmds="git docker"

_giturl="https://gitee.com/tinylab/cloud-lab"
_launcher_url="https://gitee.com/nebell/shell-script-test/raw/master/cloud-lab.sh"

main() {

    if [ "$(id -u)" = 0 ]; then
        err "Please do not run as root."
        exit 255
    fi

    if [ "$1" = "uninstall" ]; then
        uninstall_cloudlab
        exit $?
    elif [ -e "$_rootdir" ]; then
        log "$_rootdir is existed, please run cloud-lab-setup.sh uninstall first."
        exit 255
    fi

    # Print system info
    local _dist _ver
    get_platform
    _ostype="$RETVAL"
    log "Current OS: $_ostype"

    # Check deps
    local input lacks
    check_lack $_need_cmds
    lacks=$RETVAL
    if [ ! "" = "$lacks" ]; then
        log "Need deps:$lacks"
        read -r -p "Install the deps [Y/N] " input
        if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
            # If in Linux, check the distribution
            if [ "$_ostype" = "linux" ]; then
                need_cmd sudo
                _install_deps_of_linux
            elif [ "$_ostype" = "darwin" ]; then
                _install_deps_of_darwin
            else
                err "Unsupported OS $_ostype"
            fi
        else
            log "Please install$lacks and try again."
            exit 255
        fi
    fi

    install_cloudlab
}

get_platform() {
    local _ostype _distname
    _ostype="$(uname -s)"

    case $_ostype in
        Linux)
            RETVAL="linux"
        ;;

        Darwin)
            RETVAL="darwin"
        ;;

        MINGW* | *MYSY*)
            RETVAL="windows"
        ;;
        *)
            RETVAL=$_ostype
        ;;
    esac
}

get_linux_distribution() {
    
    if check_cmd source; then
        source /etc/os-release
    else
        . /etc/os-release
    fi
    RETVAL=$NAME
    RETVAL2=$VERSION
}

shorten_linux_distribution() {
    local dist
    dist="$(echo $1 | tr '[:upper:]' '[:lower:]')"
    case "$dist" in
        *buntu*)
            RETVAL="ubuntu"
        ;;
        *debian*)
            RETVAL="debian"
        ;;
        *fedora*)
            RETVAL="fedora"
        ;;
        *centos*)
            RETVAL="centos"
        ;;
        *)
            RETVAL="$1"
        ;;
    esac
}

_install_deps_of_linux() {
    get_linux_distribution
        _dist="$RETVAL"
        _ver="$RETVAL2"
        log "Linux Distribution: $_dist $_arch"

        # check and install docker meanwhile
        shorten_linux_distribution $_dist
        _dist="$RETVAL"
        case "$_dist" in
            ubuntu | debian)
                need_cmd apt-get
                _update_cmd="sudo apt-get update -y"
                _install_cmd="sudo apt-get install -y"
                _deps="$_basic_deps apt-transport-https ca-certificates curl gnupg lsb-release"
                $_update_cmd
                ensure $_install_cmd $_deps
                if ! check_cmd docker; then
                    _config_docker_apt $_dist
                    _deps="$_deps docker-ce docker-ce-cli containerd.io"
                fi
            ;;
            
            fedora | centos)
                # Check dnf or yum
                if check_cmd dnf; then
                    _update_cmd="sudo dnf update -y"
                    _install_cmd="sudo dnf install -y"
                    ensure sudo dnf -y install dnf-plugins-core
                    ensure sudo dnf config-manager --add-repo "https://download.docker.com/linux/$_dist/docker-ce.repo"
                else
                    need_cmd yum
                    _update_cmd="sudo yum update -y"
                    _install_cmd="sudo yum install -y"
                    ensure sudo yum install -y yum-utils
                    ensure sudo yum-config-manager --add-repo "https://download.docker.com/linux/$_dist/docker-ce.repo"
                fi

                # set mirrors
                select_docker_mirror
                if [ ! "$RETVAL" = "download.docker.com" ]; then
                    ensure sudo sed -i "s+download.docker.com+$RETVAL+" /etc/yum.repos.d/docker-ce.repo
                fi

                _deps="$_basic_deps docker-ce docker-ce-cli containerd.io"

            ;;

            openSUSE*)
                need_cmd zypper
                _update_cmd="sudo zypper update -y"
                _install_cmd="sudo zypper install -y"
                _deps="$_basic_deps docker"

            ;;

            "Arch Linux")
                need_cmd pacman
                _install_cmd="sudo pacman -Syy"
                _deps="$_basic_deps docker"

            ;;

            *)
                log "Please install git and docker."
                need_cmd "$_basic_deps docker"
            ;;

        esac

        # Install deps
        check_lack $_deps
        $_update_cmd
        ensure $_install_cmd $RETVAL
}

_install_deps_of_darwin() {
    need_cmd brew
    if [ -f "/Applications/Docker.app/Contents/Resources/bin/docker" ]; then
        log "Docker was installed."
        check_lack "git sudo"
    else
        check_lack "git docker sudo"
    fi

    lacks=$RETVAL
    if [ ! "" = "$lacks" ]; then
        brew install $lacks
        log "Please add docker in \$Path."
        log "If command 'docker' not found, please try: "
        log 'cat > /etc/profile << EOF
        DOCKER_PATH="/Applications/Docker.app/Contents/Resources/bin/docker"
        PATH="$PATH:$DOCKER_PATH"
        EOF'
        if [ ! $? = 0 ]; then
            err "Failed to install git or docker."
        fi
    fi
}

install_cloudlab() {
    need_cmd "$_basic_deps" install

    # check the build directory, if not existed, mkdir one
    if [ ! -d "$_builddir" ]; then
        ensure mkdir -p "$_builddir"
    fi

    cd "$_builddir"
    if [ ! -d "$_name" ]; then
        ensure git clone "$_giturl" "$_name"
    else
        cd "$_name" && git pull
    fi

    # Launcher
    local _dld
    check_cmd wget
    if [ ! $? = 0 ]; then
        need_cmd curl
        _dld="curl $_launcher_url -sSL -o"
    else
        _dld="wget $_launcher_url -q -O"
    fi

    log "Downloading launcher..."
    $_dld "$_builddir/cloud-lab.sh"
    if [ ! -f "$_builddir/cloud-lab.sh" ]; then
        err "Failed to download launcher."
    fi

    # Install
    if [ "$_ostype" = "darwin" ]; then
        # _rootdir="/usr/local/opt/tinylab/"
        _rootdir="/Users/`whoami`/Documents/tinylab/"
        ensure sudo install -d -o "$USER" "$_rootdir"
        ensure sudo hdiutil create -type SPARSE -size 60g -fs "Case-sensitive Journaled HFS+" -volname labspace "$_rootdir/labspace.dmg"
        ensure sudo hdiutil attach -mountpoint "$_rootdir/$_name" -nobrowse "$_rootdir/labspace.dmg.sparseimage"
        ensure sudo cp -rp "$_builddir/$_name/." "$_rootdir/$_name"

    else

        ensure sudo install -d -o "$USER" "$_rootdir"
        ensure sudo cp -rp "$_builddir/$_name/." "$_rootdir"
        ensure sudo install -Dm644 "$_builddir/$_name/COPYING" "/usr/share/licenses/$_name/LICENSE"
        ensure sudo install -Dm755 "$_builddir/cloud-lab.sh" "/usr/bin/cloud-lab"
    fi

    if [ ! -d "$_rootdir" ]; then
        err "Failed."
    else
        log "Installed $_name in $_rootdir."
    fi
    rm -rf "$_builddir"
}


# Check the lack of deps by `command -v`
check_lack() {
    # Find out the lack of deps
    local lacks
    lacks=""
    for dep in $@; do
        if ! check_cmd "$dep"; then
            lacks="$lacks $dep"
        fi
    done

    RETVAL=$lacks
}

uninstall_cloudlab() {
    sudo rm -rf "$_rootdir"
    sudo rm -f "/usr/bin/cloud-lab"
    sudo rm -f "/usr/share/licenses/$_name/LICENSE"
}

# Install docker by adding apt repo
_config_docker_apt() {

    # requirements to install docker
    need_cmd sudo apt-get curl tr

    # turn distribution into lower letter
    # _dist="$(echo $1 | tr '[:upper:]' '[:lower:]')"
    _dist="$1"
    _arch="$(uname -m)"
    if [ "$_arch" = "x86_64" ]; then
        _arch="amd64"
    fi

    select_docker_mirror
    _apt_repo_url="https://$RETVAL/linux/$_dist/"

    # gpg
    curl -fsSL "$_apt_repo_url/gpg" | sudo gpg --dearmor -o "/usr/share/keyrings/docker-archive-keyring.gpg"

    ensure echo \
    "deb [arch=$_arch signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] $_apt_repo_url \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
}

# Select mirrors of docker repo
select_docker_mirror() {
    # choose source url of docker
    local input
    echo "Please choose mirror to download docker"
    echo "[0] download.docker.com (Origin)"
    echo "[1] mirrors.tuna.tsinghua.edu.cn"
    echo "[2] mirrors.ustc.edu.cn"
    read -p "==> " input
    case $input in
        1)
            RETVAL="mirrors.tuna.tsinghua.edu.cn/docker-ce"
        ;;

        2)
            RETVAL="mirrors.ustc.edu.cn/docker-ce"
        ;;

        *)
            RETVAL="download.docker.com"
        ;;
    esac
}

# Utils

need_cmd() {
    for cmd in $@; do
        if ! check_cmd $cmd; then err "need '$cmd' (command not found)"; fi
    done
}

check_cmd() {
    command -v "$1" > /dev/null 2>&1
    return $?
}

log() {
    printf 'LOG: %s\n' "$1"
}

err() {
    log "$1" >&2
    exit 255
}

ensure() {
    if ! "$@"; then 
        err "command failed: $*"
    fi
}

main "$@" || exit 255